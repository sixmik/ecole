module com.example.exemplejavafx {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.exemplejavafx to javafx.fxml;
    exports com.example.exemplejavafx;
}