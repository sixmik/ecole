public class Main {

    // Une méthode ne peut retourner ainsi un résultat d'un calcul.
    public static void addition(int arg1, int arg2, int retour) {
        retour = arg1 + arg2;
    }

    public static int addition(int arg1, int arg2) {
        return arg1 + arg2;
    }

    public static void main(String[] args) {

        int valeur = 0;

        // Java passe les arguments par valeur et non par référence. Donc une méthode reçoit une copie de l'objet et non l'objet lui-même.
        addition(3,4, valeur);

        // Java peut retourner une valeur.
        valeur = addition(3,4);

        System.out.println(valeur);

    }
}